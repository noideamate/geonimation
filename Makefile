CC = gcc
CFLAGS = -std=c11 -Wall -Wpedantic
LFLAGS =
HDRDIR = hdr
SRCS = $(wildcard src/*.c)
OBJS = $(patsubst src/%.c,obj/%.o,$(SRCS))

geonimation: $(OBJS)
	$(CC) -o $@ $^

obj/%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $^ -I$(HDRDIR)

.PHONY: build debug clean release
build: geonimation

debug: CFLAGS += -g -DDEBUG
debug: build

release: CFLAGS += -O2 -DRELEASE
release: build

clean:
	${RM} geonimation obj/*.o
